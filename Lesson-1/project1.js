import React, { Component } from 'react'
import {
  View,
  Text
} from 'react-native'

export default class App extends Component {

  state = {
    title: "David Lucas"
  }

  componentDidMount() {
    var name = ""
    name = "Jeremy Francis"

    this.setState({
      title: name
    })
  }

  render() {
    return (
      <View style={Styles.container}>
        <Text style={Styles.title}>{this.state.title}</Text>
      </View>
    )
  }
}

const Styles = {
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  title: {
    fontSize: 22
  }
}

